import React from 'react';
import { TeamProps } from './Team.props';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/swiper-bundle.min.css';
import 'swiper/swiper.min.css';

import styles from './Team.module.scss';
import { Grid } from '../../Atoms/Grid/Grid';
import { Text } from '../../Atoms/Text/Text';
import useMobile from '../../../hooks/useMobile';

const slides = [
  {
    name: 'Boom',
    link: '' ,
    role: 'Co-Founder, negotiations and strategy, amazing at scaling small startups to 7 figures brands',
  },
  {
    name: 'Nighteyewear',
    // link: <a style={{color:'#0095ff',}} target = "_blank" href="https://twitter.com/Nighteyewear"><img src="socials\twitter.svg" alt="" /></a>,
    role: 'Co-Founder, strategy, marketing and tokenomics, obsessed by healthy nutrition and sports',
  },
  {
    name: 'Alex Green',
    // link: <a style={{color:'#0095ff',}} target = "_blank" href="https://twitter.com/Alex_Green_Bull"><img src="socials\twitter.svg" alt="" /></a>,
    role: 'Co-Founder, operations, marketing and team building, over 5 years experience in the crypto space',
  },
  {
    name: 'Tio Geres',
    // link: <a style={{color:'#0095ff',}} target = "_blank" href="https://www.linkedin.com/in/tio-geres-69a11939/"><img className='imgLinkedin' src="socials\linkedin.svg" alt="" /></a>,
    role: 'Smart contract developer, built over 10 crypto projects in the past',
  },
  {
    name: 'GD',
    role: 'Discord moderator, experience in selling out NFT collections',
  },
  {
    name: 'Thomas',
    role: 'Product Manager, experience in selling out NFT collections',
  },
  {
    name: 'Kaedone',
    role: 'Digital marketing, responsible for strategy and Twitter, experienced in performance advertising',
  },
  {
    name: 'Ioan',
    role: 'Lead Designer, 3D design and graphics, owner of a design company',
  },
  {
    name: 'Xromuc',
    role: 'Community manager',
  },
  {
    name: 'Honey Bun',
    role: 'Human resources manager, recruiting and taking care of the team, cycling amateur',
  },
];
let slider;

export const Team = ({ ...props }: TeamProps): JSX.Element => {
  const mobile = useMobile();
  return (
    <div id={'team'} className={styles.carousel} {...props}>
      <div style={{ padding: 'calc(var(--index) * 5) 0' }}>
        <Grid between style={{ padding: !mobile ? '0 calc(var(--index) * 5)' : '0 calc(var(--index) * 2)' }}>
          <Text heading3 gradient>
            Team
          </Text>

          <Grid autoX>
            <img
              onClick={() => slider.slidePrev()}
              className={`${styles.button} ${styles.invert}`}
              src="/slider-button.svg"
              alt="Slider Prev"
            />

            <img onClick={() => slider.slideNext()} className={styles.button} src="/slider-button.svg" alt="Slider Next" />
          </Grid>
        </Grid>
      </div>

      <Swiper
        onInit={(swiper) => {
          slider = swiper;
        }}
        spaceBetween={mobile ? 160 : 20}
        slidesPerView={3}
        initialSlide={1}
        centeredSlides
        slideToClickedSlide
        onSlideChange={() => console.log('slide change')}>
        {slides.map(({ name, role, link, }, index) => {
          return (
            <SwiperSlide key={index} style={{ left: '-25vw', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              <Grid column centerX centerY style={{ width: '90%', textAlign: 'center' }}>
                <img
                  style={{
                    width: !mobile ? '100%' : 'calc(var(--index) * 12)',
                    marginBottom: 'calc(var(--index) * 1)',
                  }}
                  src={`/team/${name.toLowerCase().replace(' ', '-')}.png`}
                  alt={`${name} image`}
                />
                <Text body3 bold uppercase style={{ fontSize: '150%' }}>
                  {name}
                </Text>
                <Text body3 bold uppercase style={{ fontSize: '150%' }}>
                  {link}
                </Text>
                <Text body2 uppercase style={{ fontSize: '90%', width: '20vw' }}>
                  {role}
                </Text>
              </Grid>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
};
